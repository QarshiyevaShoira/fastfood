/* eslint-disable import/no-unresolved */
import Vue from 'vue';
import VueRouter from 'vue-router';
import HomeView from '@/views/HomeView';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
  },
  {
    path: '/contact',
    name: 'Contact',
    component: () => import('@/views/Contact'),
  },
  {
    path: '/katalog',
    name: 'KatalogView',
    component: () => import('@/views/Katalog/Katalog.vue'),
  },
  {
    path: '/korzina',
    name: 'korzina',
    component: () => import('@/views/Karzina/Karzina.vue'),
  },
  {
    path: '/praduct/:id',
    name: 'CardAbout',
    component: () => import('@/components/CardAboute'),
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: () => import('@/views/404view/404view.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
